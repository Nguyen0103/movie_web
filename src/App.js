import logo from './logo.svg';
import './App.css';
import LoginPage from './Pages/LoginPage/LoginPage';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage/HomePage';
import { Component } from 'react';
import Layout from './HOC/Layout';
import DetailPage from './Pages/DetailPage/DetailPage';
import SpinnerComponent from './Components/SpinnerComponent/SpinnerComponent';

function App() {
  return (
    <div className=''>
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/detail/:id" element={<Layout Component={DetailPage} />} />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
