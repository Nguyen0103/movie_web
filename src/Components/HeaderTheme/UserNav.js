import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { loginAction } from '../../redux/actions/UserAction'
import { localStorageServ } from '../../services/logcalStorageService'

export default function UserNav() {

    let dispatch = useDispatch()
    let userInfo = useSelector((state) => {
        return state.userReducer.userInfo
    })

    const renderContent = () => {
        if (userInfo) {
            return (
                <div className='flex space-x-5 items-center'>
                    <p>{userInfo.hoTen}</p>
                    <button
                        onClick={hanleLogout}
                        className='border-red-500 text-red-500 rounded px-5 py-3 border-2'>Đăng xuất</button>
                </div>
            )
        } else {
            return (
                <div className='flex space-x-5 items-center'>
                    <button
                        onClick={() => {
                            window.location.href = '/login'
                        }}
                        className='border-blue-500 text-blue-500 rounded px-5 py-3 border-2'>Đăng nhập</button>
                    <button className='border-orange-500 text-orange-500 rounded px-5 py-3 border-2'>Đăng ký</button>
                </div>
            )
        }
    }

    const hanleLogout = () => {
        localStorageServ.user.remove()
        dispatch(loginAction(null))
    }

    return (<div>{renderContent()}</div>)
}
