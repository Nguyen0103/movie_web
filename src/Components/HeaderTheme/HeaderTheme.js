import React from 'react'
import UserNav from './UserNav'

export default function HeaderTheme() {
    return (
        <div className='h-20 px-10 flex justify-between items-center shadow-lg'>
            <div style={{ color: "#023047" }}
                className="logo text-2xl font-medium">
                Logo
            </div>

            <UserNav />
        </div >
    )
}
