import { movieService } from "../../services/movieService"
import { SET_MOVIE_LIST } from "../constant/movieConstant";



export const getMovieListActionServ = () => {
    return (dispatch) => {
        movieService.getMovieList()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: SET_MOVIE_LIST,
                    payload: res.data.content,
                })
            })
            .catch((err) => {
                console.log(err);
            });
    }
}