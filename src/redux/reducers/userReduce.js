import { localStorageServ } from "../../services/logcalStorageService";

const initialState = {
    userInfo: localStorageServ.user.get(),
}

export let userReducer = (state = initialState, action) => {
    switch (action.type) {

        case "LOGIN": {
            state.userInfo = action.payload
            return { ...state };
        }

        default:
            return state;
    }
}
