import React from 'react'
import { Form, Input, message } from 'antd';
import { userService } from '../../services/userService';
import { localStorageServ } from '../../services/logcalStorageService';
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { loginAction } from '../../redux/actions/UserAction';
import LoginAnimate from './LoginAnimate';

export default function LoginPage() {

    let dispatch = useDispatch()
    let history = useNavigate();

    const onFinish = (values) => {
        console.log('Success:', values);

        //back lại trang trước
        // history(-1)

        userService.postLogin(values)
            .then((res) => {
                message.success('Đăng nhập thành công')
                //lưu lên redux
                // dispatch(setUserInfor(res.content))
                //lưu vào localStorage
                localStorageServ.user.set(res.content)
                // chuyển trang => sẽ load lại trang
                // window.location.href = "/";

                //chuyển trang => không load lại trang
                setTimeout(() => {
                    history('/')
                }, 1000)
            })
            .catch((err) => {
                message.error(err.response.data.content)
            });
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className='bg-red-400 h-screen w-screen p-10'>
            <div className='container mx-auto bg-white rounded-xl p-10 flex'>
                <div className='w-1/2 h-96'>
                    <LoginAnimate />
                </div>
                <div className='w-1/2'>
                    <Form
                        name="basic"
                        layout='vertical'
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 24,
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label={<p className='text-blue-500'>Tài khoản</p>}
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tài khoản !',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={<p className='text-blue-500'>Mật khẩu</p>}
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu không để rỗng!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <div className='flex justify-center'>
                            <button

                                className='rounded px-5 py-2 text-white bg-red-500'>
                                Đăng nhập
                            </button>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    );
}
