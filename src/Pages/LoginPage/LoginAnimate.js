import React from 'react'
import Lottie from 'lottie-react'
import bgAnimate from '../../assets/loginAnimate.json'

export default function LoginAnimate() {
    return (
        <div className='transform -translate-y-16'>
            <Lottie animationData={bgAnimate} loop={true} height={300} />
        </div>
    )
}
