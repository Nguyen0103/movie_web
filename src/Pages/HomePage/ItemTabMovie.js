import moment from 'moment'
import React from 'react'

export default function ItemTabMovie({ phim }) {
    return (
        <div className='flex space-x-10 p-5'>
            <img className='w-24 my-2 h-48 object-cover' src={phim.hinhAnh} alt="" />
            <div>
                <p className='font-medium mb-5 text-2xl text-gray-800'>{phim.tenPhim}</p>
                <div className='grid grid-cols-4 gap-4'>
                    {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieuPhim) => {
                        return (
                            <div className='bg-red-600 text-white p-2 rounded'>
                                {moment(lichChieuPhim.ngayChieuGioChieu).format('DD-MM-YYYY')}
                                <span className='text-black font-bold text-base ml-5'>
                                    {moment(lichChieuPhim.ngayChieuGioChieu).format('hh:mm')}
                                </span>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}
