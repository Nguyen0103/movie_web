import React, { useEffect, useState } from 'react'
import { Tabs } from 'antd';
import { movieService } from '../../services/movieService';
import ItemTabMovie from './ItemTabMovie';
import SpinnerComponent from '../../Components/SpinnerComponent/SpinnerComponent';
const { TabPane } = Tabs;

export default function TabsMovie() {

    const [dataMovie, setDataMovie] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    const onChange = (key) => {
        console.log(key);
    };

    useEffect(() => {
        setIsLoading(true)
        movieService.getMovieByTheater()
            .then((res) => {
                setIsLoading(false)
                setDataMovie(res.data.content)

            })
            .catch((err) => {
                setIsLoading(false)
            });
    }, [])

    const renderContent = () => {
        return (dataMovie.map((heThongRap, index) => {
            return (
                <TabPane tab={<img className='w-16' src={heThongRap.logo} />} key={index} >
                    <Tabs style={{ height: 500 }} tabPosition='left' defaultActiveKey="1" onChange={onChange} >
                        {heThongRap.lstCumRap.map((cumRap, index) => {
                            return (
                                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                                    <div style={{ height: 500, overflowY: 'scroll' }} className='shadow'>
                                        {cumRap.danhSachPhim.map((phim, index) => {
                                            return <ItemTabMovie phim={phim} />
                                        })}
                                    </div>
                                </TabPane>
                            )
                        })}
                    </Tabs >
                </TabPane >
            )
        }))
    }

    const renderTenCumRap = (cumRap) => {
        return (
            <div className='text-left w-60'>
                <p className='text-green-700 truncate'>{cumRap.tenCumRap}</p>
                <p className='text-black truncate'>{cumRap.diaChi}</p>
                <button className='text-red-600'>[Xem chi tiết]</button>
            </div>
        )
    }

    return (
        <div className='container py-20 mx-auto pb-96'>
            {/* {isLoading ? <SpinnerComponent /> : ""} */}
            <Tabs tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                {renderContent()}
            </Tabs>
        </div>
    )
}
