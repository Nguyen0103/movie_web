import { Card } from 'antd';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getMovieListActionServ } from '../../redux/actions/MovieAction';
import { movieService } from '../../services/movieService'
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function ListMovie() {

    let dispatch = useDispatch()

    let { movieList } = useSelector((state) => {
        return state.movieReducer
    })
    console.log('movieList: ', movieList);

    useEffect(() => {
        // movieService.getMovieList()
        //     .then((res) => {
        //         console.log(res);
        //     })
        //     .catch((err) => {
        //         console.log(err);
        //     });
        dispatch(getMovieListActionServ())
    }, [])

    const renderMovieList = () => {
        return movieList
            .filter((item) => {
                return item.hinhAnh !== null
            })
            .map((item) => {
                let { hinhAnh, tenPhim, biDanh, maPhim } = item
                return (
                    <Card
                        hoverable
                        style={{
                            width: '100%',
                        }}
                        className="shadow-xl hover:shadow-slate-800 hover:shadow-xl"
                        cover={<img alt={tenPhim} src={hinhAnh} />}
                    >
                        <Meta title={<p className='text-blue-500'>{biDanh}</p>} description="www.instagram.com" />

                        <NavLink to={`detail/${maPhim}`}>
                            <button className='w-full bg-red-500 text-white rounded text-xl font-medium py-3'>Mua vé</button>
                        </NavLink>
                    </Card >
                )
            })
    }

    return (
        <div className='grid grid-cols-4 gap-5 container mx-auto mt-5'>
            {renderMovieList()}
        </div>
    )
}
