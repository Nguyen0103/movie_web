import React from 'react'
import ListMovie from './ListMovie'
import TabsMovie from './TabsMovie'

export default function HomePage() {
    return (
        <div className='space-y-20'>
            <ListMovie />
            <TabsMovie />
        </div>
    )
}
