import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { movieService } from '../../services/movieService'
import { Progress } from 'antd';


export default function DetailPage() {
    let { id } = useParams()
    const [movie, setMovie] = useState({})

    useEffect(() => {
        movieService.getMovieDetail(id)
            .then((res) => {
                setMovie(res.data.content)
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    return (
        <div className='container mx-auto'>
            <div className="flex justify-center items-center py-10 space-x-10">
                <img src={movie.hinhAnh} alt="" className='w-60' />
                <p className='text-3xl text-red-500 font-medium'>
                    {movie.biDanh}
                </p>
                <Progress type="circle" percent={movie.danhGia * 10}
                    format={(number) => {
                        return (
                            <span className='text-black font-medium'>
                                {number / 10} Điểm
                            </span>
                        )
                    }} />
            </div>
        </div>
    )
}
